@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="console.log console.log-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="/createPassword">
                        {{ csrf_field() }}

                        @if (!auth()->user()->verified())

                        <input type="hidden" id="validateTokenRoute" value="{{route('vt')}}">

                        <div class="form-group">
                            <label for="token" class="col-md-4 control-label">Token</label>

                            <div class="col-md-6">
                                <input id="token" type="text" class="form-control" name="token" value="" required>
                            </div>
                        </div>
                        @endif
                   
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Create Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script>
    @if (!auth()->user()->verified())
        
        $(document).ready(function() {            
            
            $("#token").blur(function() {

                var token = $("#token").val();
                var route = $('#validateTokenRoute').val();
                
                    $.get(route, token, function(data) {
                    $(token).html(data);
                    
                    if (data.trim() == "true") {

                        console.log( "Token valid!" );
                        $("#token").prop('readonly', true);
                        $("#password").prop("readonly", false);
                        $("#password-confirm").prop("readonly", false);
                    } else
                    {
                        alert( "Token invalid!" );
                    }
                }).fail(function(){
                    console.log("error!!!!!");
                });
            }); 
        });
    @else
        $(document).ready(function() {
            $("#password").prop("readonly", false);
            $("#password-confirm").prop("readonly", false);
        });  
    @endif
     
</script>
@endsection
