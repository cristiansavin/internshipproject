<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// show login 
Route::get('/', 'PagesController@loginPage');

// show home 
Route::get('/home', 'HomeController@index')->name('home');

// show confirmRegistration view
Route::get('/confirmRegistration', 'PagesController@confirmRegistration')->name('confirmRegistration');

// resend token
Route::get('/resendToken', 'PagesController@resendToken')->name('resendToken');

// delete account
Route::post('/resendToken', 'CustomAuthController@deleteAccount');

// show createPassword view
Route::get('/verify', 'PagesController@createPassword');

// verify the user if clicks the link with token
Route::get('/verify/{token}', 'CustomAuthController@verify')->name('verify');

// verify the user if clicks the link without token
Route::get('/validateToken', 'CustomAuthController@validateToken')->name('vt');

// show createPassword view
Route::get('/createPassword', 'PagesController@createPassword')->name('createPassword');

// route for saving the password in database
Route::post('/createPassword', 'CustomAuthController@store');
