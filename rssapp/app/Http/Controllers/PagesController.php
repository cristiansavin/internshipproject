<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function loginPage()
    {
        return view('auth/login');
    }

    public function confirmRegistration()
    {
        return view('confirmRegistration');
    }


    public function createPassword()
    {
        return view('createPassword');
    }

   
    public function resendToken()
    {
        return view('resendToken');
    }
}
