<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\User;


class CustomAuthController extends Controller
{

    
    public function validateToken()
    {
        
        $token = url()->full();  //get url with query string
        $token = explode('?',$token); //convert from string to array
        
        $token = $token[1];  // get token from array
        
        $tokenDatabase = DB::table('users')
                                 ->where('token', $token)
                                 ->select('token')->get()->first();
        if (!$tokenDatabase) {
            echo "false";
            return; 
        }  

        $tokenCreatedAt = DB::table('users')
                                ->where('token', $token)
                                ->select('token_created_at')->get();

        // set expiration date for token
        $tokenCreatedAt = strtotime('+60 minutes');
        $now = strtotime('now');
        $compareStrings = strcmp($token, $tokenDatabase->token);

        // compare tokens and decide if token has expired
        if( $compareStrings == 0 && $tokenCreatedAt > $now)
        {
            // verify the user and update token to null
            DB::table('users')
                    ->where('token', $token)->update(['token' => null]);   // update token
            echo "true";
            return;
        } else
        {
            echo "false";
            return;
        }
    }

    /**
     * Verify the user with a given token
     * @param string $token
     * @return Response
     */
    public function verify($token)
    {
        $tokenCreatedAt = User::where('token', $token)
                                    ->select('token_created_at')->get();

        // set expiration date for token
        $tokenCreatedAt = strtotime('+60 minutes');
        $now = strtotime('now');

        // redirect the user if token expired or not
        if($tokenCreatedAt > $now)
        // if (validateToken())
        {
            // verify the user and update token to null
            User::where('token', $token)->firstOrFail()
                     ->update(['token' => null]);   // update token

            // if account is verified, redirect the user:
            return redirect()
                    ->route('createPassword')
                    ->with('success', 'Account verified!');
        }
        else
        {
            regenerateToken();
            return redirect()
                    ->route('resendToken')
                    ->with('success', 'A new token was send to your email addres!');
        }
    }


    public function regenerateToken()
    {
        // generate another token
        User::where('token', $token)
                ->update(['token' => str_random(50)]);

        // send another email with the new token
        $user->sendVerificationEmail();

        // update "token_created_at" in database
        DB::table('users')
                ->where('token_created_at')
                ->update(['token_created_at' => now()]);
    }

    // if the token is not valid anymore, the user can choose to delete his account
    public function deleteAccount()
    {
        $user = Auth::user();
       
        DB::table('users')->where('id', $user->id)->delete();
        
        // redirect the user to Register page
        return redirect()
                    ->route('register')
                    ->with('success', 'Account deleted! Good luck!');
    }
    

    // save the password to database
    public function store(Request $request)
    {
        // get the user making the request
        $user = $request->user();
        
        $this->validate($request, [
            // rules:
            'password' => 'required|string|min:6|confirmed',
        ]);
        // encrypt password:
        $password = encrypt($request->input['password']);
        
        // add password to database
        $data = array('password'=> $password);
        DB::table('users')
                ->where('id', $user->id)
                ->update($data);
             
        // redirect home
        return redirect()
        ->route('home')
        ->with('success','Registration completed!');
    }

}
